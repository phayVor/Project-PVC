// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2019 00:00:00").getTime();

// Update the count down every 1 second
var countdownfunction = setInterval(function() {
  // Get todays date and time
  var now = new Date().getTime();
  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML =
    days + " days " + hours + " hours " + minutes + " min " + seconds + " sec";

  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(countdownfunction);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);

// Registration model
// get the modal
let modal = document.getElementById("PVC_reg");
let btn = document.getElementById("getReg");
let close = document.getElementsByClassName("close")[0];

// open modal
btn.onclick = function() {
  modal.style.display = "block";
};

// close the modal
close.onclick = function() {
  modal.style.display = "none";
};

window.onclick = function(e) {
  if (e.target == modal) {
    modal.style.display = "none";
  }
};

// functionality for form filling
let currentTab = 0;
showTab(currentTab);

function showTab(n) {
  let tab = document.getElementsByClassName("tab");
  tab[n].style.display = "block";

  // the prev and next buttons
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }

  if (n == tab.length - 1) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  fixStepIndicator(n);
}

function nextPrev(n) {
  let tab = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;

  // hide the current tab
  tab[currentTab].style.display = "none";
  currentTab += n;

  if (currentTab >= tab.length) {
    // submit form
    document.getElementById("regForm").submit();
    return false;
  }
  // else show correct tab
  showTab(currentTab);
}

function validateForm() {
  let x,
    y,
    i,
    valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");

  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  let i,
    x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //  and adds the 'active' class to the current step:
  x[n].className += " active";
}

// goto the assurance page
(function getAssurance() {
  let getAssur = document.getElementById("getAssurance");

  getAssur.addEventListener("click", function () {
    // window.location.href = "realquestions.html";
    window.location.href = 'industrious/q1.html';
  });
}());

// goto lazy nigerian page
(function lazy() {
  let lazyBut = document.getElementById("gameOn");

  lazyBut.addEventListener('click', function () {
    window.location.href = 'lazyquestions/lazy/q1.html';
  });
}());